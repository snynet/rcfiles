#!/bin/bash

GREEN='\033[0;32m'
NC='\033[0m' # No Color

SCRIPTDIR=$(readlink -e $(dirname $0))

command -v git 2>&1 > /dev/null || { echo "Need git"; exit 1; }
command -v vim 2>&1 > /dev/null || { echo "Need vim"; exit 1; }

# sudo without password
echo -e "${GREEN}Enable sudo without password.  I'll visudo, then you add this to the end:${NC}"
echo "${USER} ALL=(ALL) NOPASSWD: ALL"
echo -e "${GREEN}Okay, ready?${NC}"
read
sudo visudo

# Get tools we commonly use that we can't get from apt-get or dnf
cd ~
if [ ! -e ~/.fzf ] ; then
    git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
    ~/.fzf/install --key-bindings --completion --update-rc
fi
if [ ! -e ~/.git-completion.bash ] ; then
    wget -O ~/.git-completion.bash \
        https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash
fi
if [ ! -e ~/.git-prompt.sh ] ; then
    wget -O ~/.git-prompt.sh \
        https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh
fi
if [ ! -e ~/.vim/bundle/vundle ] ; then
    git clone --depth 1 https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
fi

# Make .bashrc eventually try to source ~/.bashrc_personal
grep -q bash_aliases ~/.bashrc
if [ $? -ne 0 ] ; then
    echo -e "\n. ~/.bash_aliases" >> ~/.bashrc
fi
if [ ! -e ~/.bash_aliases ] ; then
    echo -e "#!/bin/bash\n\n" > ~/.bash_aliases
fi
grep -q bashrc_personal ~/.bash_aliases
if [ $? -ne 0 ] ; then
    echo "if [ -e ${SCRIPTDIR}/bashrc_personal ] ; then . ${SCRIPTDIR}/bashrc_personal ; fi" \
        >> ~/.bash_aliases
fi

# Preferred .rc stuff
cd ~
ln -sf ${SCRIPTDIR}/psqlrc .psqlrc
ln -sf ${SCRIPTDIR}/tmux.conf .tmux.conf
ln -sf ${SCRIPTDIR}/vimrc .vimrc
ln -sf ${SCRIPTDIR}/ideavimrc .ideavimrc
ln -sf ${SCRIPTDIR}/inputrc .inputrc
ln -sf ${SCRIPTDIR}/alacritty.yml .alacritty.yml
ln -sf ${SCRIPTDIR}/gitconfig .gitconfig
mkdir -p .config/zathura
ln -sf ${SCRIPTDIR}/zathurarc .config/zathura/zathurarc
if [ -e .Xresources ] ; then
    echo "Looks like there's already a .Xresources, so you're on your own for that one."
else
    ln -sf ${SCRIPTDIR}/Xresources .Xresources
fi

# Vim snippets
mkdir -p ${HOME}/.vim/snippets
ln -sf ${SCRIPTDIR}/tex.snippets ${HOME}/.vim/snippets/tex.snippets

# Finish getting Vim ready
vim +PluginInstall +qall

echo -e "${GREEN}All done.${NC}"
