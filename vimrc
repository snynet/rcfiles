"""""""""
" GENERAL
"""""""""

" Use jj instead of escape to get out of insert mode
:imap jj <Esc>

" Make backspace work how you might expect
set backspace=indent,start,eol

" Typography
set tabstop=4
set shiftwidth=4
set expandtab

" Nowadays, we have a bunch of colors, but not everything realizes that.
set term=xterm-256color

" Swap file stuff sometimes causes frequent short freezes, and power outages don't happen very often here
set noswapfile

" Yank whole buffer to system clipboard (e.g., when you need to put something long-winded in a simplistic text box)
nnoremap <Leader>cp :%y+<CR>

" You very often seem to want to just jump to the end of the line from deep in a nest of pairs
inoremap <Leader>$$ <Esc>$a<Space>

"""""""""""""""""""""""""""""""""""
" USEFUL FOR BUILDING CODE FROM VIM
"""""""""""""""""""""""""""""""""""

" Make quickfix not jump to first finding right away; botright makes the quickfix window span multiple vertical splits
autocmd QuickFixCmdPost [^l]* nested botright cwindow
autocmd QuickFixCmdPost l* nested lwindow

"
" Useful for working with code
"

filetype plugin on

" Adapted from http://vim.wikia.com/wiki/Insert_multiple_lines Open multiple lines (insert empty lines) before or after
" current line, and position cursor in the new space, with at least one blank line before and after the cursor.
function! OpenLines(nrlines, dir)
    let nrlines = a:nrlines < 2 ? 2 : a:nrlines
    let start = line('.') + a:dir
    call append(start, repeat([''], nrlines))
    if a:dir < 0
        normal! 2k
    else
        normal! 2j
    endif
endfunction
" Mappings to open multiple lines and enter insert mode.
nnoremap <Leader>o :<C-u>call OpenLines(v:count, 0)<CR>S
nnoremap <Leader>O :<C-u>call OpenLines(v:count, -1)<CR>S

" Don't try to complete from /usr/include
set path=.

" Completion automatically select the first option.  It's often what I want.
set completeopt-=longest

set foldmethod=indent

" Automatic formatting
filetype indent on
" Only indent once following newline after unclosed parenthesis
set cinoptions+=(1s
set cinoptions+=N-s

" Indent list items in Markdown more nicely
autocmd Filetype markdown set autoindent
" Especially useful with Markdown lists: indent/unindent in insert mode
inoremap <Leader>>> <Esc>>>a
inoremap <Leader><< <Esc><<a

" Highlight matching angle brackets
set matchpairs+=<:>

" Ctrl-k <letter>s for subscript
" https://vi.stackexchange.com/a/7436, with a riff
"alphsubs ---------------------- {{{
        execute "digraphs _k " . 0x2096
        execute "digraphs _a " . 0x2090
        execute "digraphs _e " . 0x2091
        execute "digraphs _h " . 0x2095
        execute "digraphs _i " . 0x1D62
        execute "digraphs _k " . 0x2096
        execute "digraphs _l " . 0x2097
        execute "digraphs _m " . 0x2098
        execute "digraphs _n " . 0x2099
        execute "digraphs _o " . 0x2092
        execute "digraphs _p " . 0x209A
        execute "digraphs _r " . 0x1D63
        execute "digraphs _s " . 0x209B
        execute "digraphs _t " . 0x209C
        execute "digraphs _u " . 0x1D64
        execute "digraphs _v " . 0x1D65
        execute "digraphs _x " . 0x2093
"}}}

"""""""""""
" SEARCHING
"""""""""""

" Highlight matches
set hlsearch
" When search is all lowercase, ignore case
set ignorecase
set smartcase
" Don't start searching till I tell you
set noincsearch

"""""""""""""""""""""
" SYNTAX HIGHLIGHTING
"""""""""""""""""""""

" https://github.com/vim/vim/issues/3608
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
set termguicolors

syntax enable

colorscheme elflord
" You also like desert and pablo

" But LaTeX files are hideous in many colorschemes.  desert and slate are okay.
autocmd BufNewFile,BufReadPost *.tex colorscheme desert
" Make sure we have a black background, though
autocmd BufNewFile,BufReadPost *.tex highlight Normal ctermbg=black guibg=black
" And by the way, it's never plaintex
autocmd BufNewFile,BufReadPost *.tex set filetype=tex

" Let Vim handle Doxygen highlighting
let g:load_doxygen_syntax = 1
let g:doxygen_javadoc_autobrief = 0 " Default seemed to be more confusing than helpful

" Custom highlighting for all languages.  Make things like parentheses,
" brackets, dereference operators, etc., show up a bit brighter than the other
" text.
function AllLangsHi()
    syn match OtherPunct /\.\|:\|->\|<\|>\|%\|*\|&\|=/
    highlight OtherPunct ctermfg=LightGreen
endfunction
autocmd Filetype * call AllLangsHi()

" For some reason, syntax highlighting doesn't always kick in for XML or HTML
autocmd Filetype xml syn on
autocmd Filetype html syn on

" Vim thinks .md means Modula-2, but we want Markdown
autocmd BufNewFile,BufReadPost *.md set filetype=markdown
let g:vim_markdown_math = 1

" We're not editing Fortran 77 files if we can help it
autocmd BufNewFile,BufReadPost *.f set filetype=fortran

" .jl means Julia, not Lisp or some such
autocmd BufNewFile,BufReadPost *.jl set filetype=julia

" Do JSON real nice
au! BufRead,BufNewFile *.json set filetype=json
let g:vim_json_syntax_conceal = 0

""""""""""""""""""""""""""""""""
" HOW TO HANDLE COMMENTS IN CODE
""""""""""""""""""""""""""""""""

" Wrap comments at 120 columns, but don't insert comment leader after hitting Enter
set textwidth=120
set formatoptions+="c"
set formatoptions-="r"
" Continue Doxygen comments in C++
function CppComments()
    set comments^=:///<,:///,:///,://!\ 
endfunction
autocmd Filetype c,cpp call CppComments()
" Continue Doxygen comments in Fortran
function FComments()
    set comments^=:!!,:!!\ 
endfunction
autocmd Filetype fortran call FComments()
autocmd Filetype html set textwidth=0
autocmd Filetype markdown set textwidth=0
autocmd Filetype tex set textwidth=0

"""""""""""""
" DECORATIONS
"""""""""""""

" Show file name and path in window title bar
set title

" A simple status bar
set laststatus=2

" LaTeX files are heavy on prose, so break in the middle of a word
autocmd Filetype tex set linebreak

""""""
" TABS
""""""

" For consistency with tmux
noremap <C-n> :tabnext<CR>
noremap <C-p> :tabprev<CR>

""""""""
" SPLITS
""""""""

" Split opening that works the similarly to personalized tmux mappings
noremap <Leader>v<Space> :vsp<CR>
noremap <Leader>s<Space> :sp<CR>

" Make the current split 120 wide
function Splitresize()
    exe "vertical resize 120" 
endfunction
noremap <Leader>rs<Space> :call Splitresize()<CR>

""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""
"
" CONFIGURATION FOR SPECIFIC PLUGINS
"
""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""

""""""""""""""
" FUZZY FINDER
""""""""""""""

set rtp+=~/.fzf

" find is more portable than ag, and 0.02 s faster in my test
let $FZF_DEFAULT_COMMAND = 'find . -type f -name "*" -not -name "*.o" -not -path "*/.git/*" -not -path "*/dist/*" -not -path "*/build/*" -not -path "*/__pycache__/*" -not -path "*/venv/*"'

" Finds directory where .git is
" https://github.com/junegunn/fzf.vim/issues/47#issuecomment-160237795
function! s:find_git_root()
    return system('git rev-parse --show-toplevel 2> /dev/null')[:-2]
endfunction

command! FzfFromGitRoot execute 'Files' s:find_git_root()

" Using Ctrl-p for something else
noremap <Leader>f<Space> :FzfFromGitRoot<CR>

""""""""
" TAGBAR
""""""""

" Quick toggle
command Tbt TagbarToggle

" Make terminals not grow (if you want them to grow, use 2 and you'll need 
" xterm*AllowWindowOps: true in .Xresources)
let g:tagbar_expand = 0

" Highlight tag we're in if you wait for a second
set updatetime=1000

"""""""
" SLIME
"""""""

let g:slime_target = "tmux"
let g:slime_default_config = {"socket_name": "default", "target_pane": "{left-of}"}
let g:slime_dont_ask_default = 1
" Ctrl-C is a funny thing to use for a hotkey.  Use something else.
xmap <Leader>ce <Plug>SlimeRegionSend
nmap <Leader>ce <Plug>SlimeParagraphSend

""""""
" JEDI
""""""

" Don't popup docstrings.  It's too jarring.  It might be nice if there was a less jarring way of doing it.
autocmd FileType python setlocal completeopt-=preview

" Don't popup when I type ".".  I'll tell you when I'd like some completions.
let g:jedi#popup_on_dot = 0

set noshowmode
let g:jedi#show_call_signatures = 2

"""""
" ALE
"""""

" Don't lint until I ask you to
let g:ale_enabled = 0
nnoremap <Leader>rl<Space> :ALEToggle<CR>

" Don't make my mistakes look like comments
let g:ale_virtualtext_cursor = "disabled"

" Not sure why Pyflakes isn't enabled by default
let g:ale_linters = {"python": ["pyflakes", "mypy"]}

"""""""""
" RAINBOW
"""""""""

let g:rainbow_active = 1

"""""""""
" ARDUINO
"""""""""

" Not sure why I need to do this
autocmd FileType arduino set autoindent

"""""""""""""""""""""""""""""""""
" SNIPMATE (and its dependencies)
"""""""""""""""""""""""""""""""""

let g:snipMate = { 'snippet_version' : 1 }

""""""""
" VUNDLE
""""""""

set rtp+=$HOME/.vim/bundle/vundle
let vundle_dir='~/.vundle_dir'
call vundle#rc(vundle_dir)
Plugin 'gmarik/vundle'

Plugin 'AndrewRadev/linediff.vim'
Plugin 'majutsushi/tagbar'
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/nerdtree'
Plugin 'tmhedberg/matchit'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-surround'
Plugin 'junegunn/fzf.vim'
Plugin 'elzr/vim-json'
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'jpalardy/vim-slime'
Plugin 'davidhalter/jedi-vim'
Plugin 'dense-analysis/ale'
Plugin 'jeetsukumaran/vim-pythonsense'
Plugin 'luochen1990/rainbow'
Plugin 'JuliaEditorSupport/julia-vim'
" SnipMate and its dependencies
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
Plugin 'garbas/vim-snipmate'
